import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, Event, NavigationStart } from '@angular/router';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { CustomValidators } from 'ng9-validation';

import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { UserService } from 'src/app/services/user.service';
import { ErrorMessages } from '../../validators/error-messages';
import { PROFILE_STU, PROFILE_TUT, MINIMUM_AGE, SERVER_DATE_FORMAT,MINIMUM_AGE_STUDENT } from 'src/app/validators/constants';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {
	public regForm: FormGroup;
	public errorMessages: any = ErrorMessages.registrationForm;
	public formErrors: any;
	public submitting: boolean;
	public userType: string = PROFILE_STU;
	public PROFILE_STU = PROFILE_STU;
	public PROFILE_TUT = PROFILE_TUT;
	private routerSubs: Subscription;
	public passwordField: string = "password";
	public passwordFieldCnf: string = "password";
	public todayDate: Date;
	public yearRange: string = this._commonService.yearsRange;
	public dobYearRange: string = this._commonService.dobYearRange;
	public isUnderAge: boolean = false;
	public underAgeMessage: any;
	public underAgeMessageData: string = 'We need the consent and approval of your parent/guardian in order to allow you to access our services. Please type the full name and email address of your parent/guardian in the input fields.';
	public resetCaptcha: boolean;

	
	constructor(private _fb: FormBuilder,
		private _router: Router,
		private _pDialogRef: DynamicDialogRef,
		private _pDialogConfig: DynamicDialogConfig,
		private _commonService: CommonService,
		private _chatService: ChatService,
		private _userService: UserService,
		private _authService: AuthService) {
		this.createForm();
		this.initFormErrors();
		if(this._pDialogConfig?.data?.type) {
			this.userType = this._pDialogConfig?.data.type;
			this.onContinue();
		} else {
			this._pDialogConfig.header = "Register";
		}
	}

	ngOnInit(): void {		
		// timeout to remove chrome auto password
		this._commonService.timeout(310).then(() => {
			this.regForm.patchValue({
				username:'',
				email:'',
				password:'',
			});
		});

		this.routerSubs = this._router.events.subscribe((event: Event) => {
			if (event instanceof NavigationStart) {
				this._pDialogRef.close();
			}
		});

		/* run validation API */
		this.regForm.get('username').valueChanges.subscribe((value) => {
			const username : any = this.regForm.get('username');
			if (username.valid && !isNaN(username)) {
				const sub : Subscription = this._userService.checkUsernameExists({
					"username": value
				}).subscribe(res => {
					const error = {
						alreadyExist: false
					}
					username.setErrors({
						alreadyExist: false
					});
					this.formErrors['username'] = error;
					sub.unsubscribe();
				}, err => {
					if(err.status == 400){
						const error = {
							alreadyExist: true
						}
						username.setErrors(error);
						this.formErrors['username'] = error;
					}
					sub.unsubscribe();
				});
			} else {
				if(!isNaN(username)) {
					this.formErrors['username'] = {};
				}
			}
		});

		this.regForm.get('email').valueChanges.subscribe((value) => {
			const email = this.regForm.get('email');
			if (email.valid && this._commonService.isValidEmail(value)) {
				const sub: Subscription = this._userService.checkEmailExists({
					"email": value
				}).subscribe(res => {
					const error = {
						alreadyExist: false
					}
					email.setErrors({
						alreadyExist: false
					});
					this.formErrors['email'] = error;
					sub.unsubscribe();
				}, err => {
						// console.log(err.status == 400);
					
					if (err.status == 400) {
						const error = {
							alreadyExist: true
						}
						email.setErrors(error);
						this.formErrors['email'] = error;
					}
					sub.unsubscribe();
				})
			}else{
				this.formErrors['email'] = {};
			}
		});
	}

	ngAfterViewInit(): void {
		try {
			(<HTMLElement>document.activeElement).blur();
		} catch (error) {}
	}

	ngOnDestroy(): void {
		this.routerSubs.unsubscribe();
	}

	public onChangeTab(activeTab : string): void {
		this.regForm.patchValue({
			type: activeTab,
		});
	}

	public onContinue(): void {
		if(this.userType == PROFILE_TUT){
			this.todayDate = moment().subtract(MINIMUM_AGE, 'years').toDate();
		}else{
			this.todayDate = moment().toDate();
		}
		this.regForm.patchValue({
			type:this.userType
		});
		if(this.userType == PROFILE_TUT) {
			this._pDialogConfig.header = "Tutor Registration";
		} else {
			this._pDialogConfig.header = "Student Registration";
		}
	}

	private createForm(): void {
		let email = new FormControl('', [Validators.required, CustomValidators.email])
		let password = new FormControl('', [Validators.required]);
		this.regForm = this._fb.group({
			type: '',
			username: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30), Validators.pattern(this._commonService.userNamePattern)]],
			email: email,
			password: password,
			acceptTos: [false, [CustomValidators.equal(true)]],
			captcha: ['', [Validators.required]]
		});
	}


	private initFormErrors() : void {
		this.formErrors = {
			username: {},
			email: {},
			password: {},
			acceptTos: {},
			captcha: {}
		}
	}

	public onSubmit(): void {
		this._commonService.checkTrimData(this.regForm);
		this.initFormErrors();
		if (this.regForm.valid) {
			let err = 0;
			if(!this.regForm.value.username.match(/[A-Za-z0-9]/g) || !isNaN(this.regForm.value.username)) {
				err++;
				this.formErrors.username = {pattern: true};
			}
			
			if(!this._commonService.isValidEmail(this.regForm.value.email)) {
				err++;
				this.formErrors.email = {email: true};
			}
			if(err == 0) {
				this.submitting = true;
				let formData : any = this.regForm.value;
				delete formData.acceptTos;

				if(this._pDialogConfig?.data?.marketing_uuid) {
					formData.marketing_uuid = this._pDialogConfig.data.marketing_uuid;
				}
				this._authService.preventReload = true;
				const subs : Subscription = this._authService.register(formData)
				.subscribe(res => {
					subs.unsubscribe();
					this.submitting = false;
					localStorage.setItem("email", this.regForm.value.email);
					/**
					 * Init chat after a user log in
					 */
					this._chatService.init();
					this._commonService.setLoggedInUser();
					this._commonService.callBack();
					if(this._pDialogConfig?.data?.marketing) {
						this._pDialogRef.close("verification");
					} else {
						this._router.navigate(['/']);
					}

					this._commonService.showSuccess(res.msg);
				}, err => {
					subs.unsubscribe();
					
					this.resetCaptcha = !this.resetCaptcha;
					this.onCaptchaResponse("");
					this.submitting = false;
					this._commonService.showError(err.error.msg);
				});
			}
		} else {
			this._commonService.checkError(this.regForm, this.formErrors)
				.then((errors: any) => {
					this.formErrors = errors;
					if(this.regForm.value.username && !this.formErrors.username.pattern && !this.regForm.value.username.match(/[A-Za-z0-9]/g)) {
						this.formErrors.username = {pattern: true};
					}
					if(this.regForm.value.username && !isNaN(this.regForm.value.username)) {
						this.formErrors.username = {pattern: true};
					}
					if(this.regForm.value.email && !this._commonService.isValidEmail(this.regForm.value.email)) {
						this.formErrors.email = {email: true};
					}
				});
			this._commonService.scrollToError();
		}
	}

	public onLogin(): void {
		this._pDialogRef.close('login');
	}

	public onSelectDate(date: Date): void {
        const dob = this.regForm.get('dob.value');
		
	   	const parentName = this.regForm.get('parentName');
	   	const parentEmail = this.regForm.get('parentEmail');
		
        const startDate = moment(dob.value, SERVER_DATE_FORMAT)
		const diffYear = moment().diff(startDate, 'years');
		
		if(diffYear < MINIMUM_AGE_STUDENT){
			this.underAgeMessage = [
				{severity:'info',  detail: this.underAgeMessageData}
			];
			this.isUnderAge = true;
			parentName.enable();
			parentEmail.enable();
		}
		else{
			parentName.disable();
			parentEmail.disable();
			this.isUnderAge = false;
		}
		this.regForm.get('isUnderAge').setValue(this.isUnderAge);		
    }

	public onCaptchaResponse(token: string): void {
		this.regForm.patchValue({
			captcha: token
		});
	}
}
