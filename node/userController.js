const moment = require('moment');

const Models = require('../../../models');
const userHelper = require('./helper');
const courseHelper = require('../courses/helper');
const notificationHelper = require('../notifications/helper');
const resourceHelper = require('../resources/helper');


const Controller = {
	/**
	 * Function to check if the given username is available or not.
	 */
	checkUserName: async (req, res, next) => {
		let _data = await userHelper.usernameExists(res, Object.assign({}, req.body, {Models: Models}));

		return AppHelpers.Utils.cRes(res, _data);
	},
	/**
	 * Function to check if the given email address is available or not.
	 */
	checkEmailAddress: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();
		let _reqData = req.body;

		// Check whether given email is valid or not
		if (!AppHelpers.Utils.isValidEmail(_reqData.email)){
			_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_EMAIL_ADDRESS })
			return AppHelpers.Utils.cRes(res, retData);
		}

		let _emailData = await Models.User.checkEmail(_reqData.email);
		if (_emailData && _emailData.length) {
			_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.EMAIL_USED })
		}
		return AppHelpers.Utils.cRes(res, retData);
	},

	/**
	 * Function to add a user.
	 */
	create: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();
		let _reqData = req.body;
		let basics = {};

		try {
			if (_reqData) {
				if (_reqData['captcha'] === undefined || _reqData['captcha'] === '' || _reqData['captcha'] === null) {
					_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.CAPTCHA_NOT_SELECTED })
					return AppHelpers.Utils.cRes(res, retData);
				}

				let _captchaValidity = await AppHelpers.VerifyCaptcha(_reqData['captcha']);

				if (_captchaValidity.status == 'error') {
					_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_CAPTCHA })
					return AppHelpers.Utils.cRes(res, retData);
				}

				if (_.isEmpty(_reqData.type) || _.indexOf(['TUT', 'STU'], _reqData.type) === -1) {
					_.assign(retData, { status: "error", statusCode: 400, msg: "Invalid user type provided." })
					return AppHelpers.Utils.cRes(res, retData);
				}

				// Check whether given email is valid or not
				if (!AppHelpers.Utils.isValidEmail(_reqData.email)){
					_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_EMAIL_ADDRESS })
					return AppHelpers.Utils.cRes(res, retData);
				}

				// check for the email with the give type/role
				let _emailData = await Models.User.checkEmail(_reqData.email);
				if (_emailData && _emailData.length) {
					_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.EMAIL_USED })
					return AppHelpers.Utils.cRes(res, retData);
				}

				// re-check for user name
				let _usernameData = await userHelper.usernameExists(res, Object.assign({}, req.body, {Models: Models}));
				if (_usernameData.status === "error") {
					return AppHelpers.Utils.cRes(res, _usernameData);
				}

				_reqData.role = [_reqData.type];
				_reqData.status = 1;

				// delete _reqData.dob;

				let _user = await Models.User.create(_reqData);

				if(_user.role[0] === 'TUT'){
					let uniqueVal = await AppHelpers.Utils.genRandomToken();
					_user.testimonials.linkCode = uniqueVal;
				}

				_user.save();

				// get ip
				let _IP = AppHelpers.Utils.getIP(req);
				// activity log
				await Models.ActivityLog.addLog('ACCADD', _user, _IP);

				// save default notification settings
				await notificationHelper.saveSettings({ userId: _user._id }, true);

				retData.msg = AppHelpers.ResponseMessages.REGISTRATION_WELCOME;

				// Generate token and send for login
				let token = await userHelper.getUserToken(_user);
				if (!token) {
					token = '';
					retData.msg = AppHelpers.ResponseMessages.REGISTERED_SUCCESSFUL_TOKEN_NOT_GENERATED;
				}

				_.assign(retData, { data: [{token: token}] });

				// send new welcome email
				let _welcomeEmailData = { to: _user.email, username: _user.username, profileHowToVideo: process.env.appURL + 'faqs' };
				if (_user.role[0] === 'TUT') {
					_welcomeEmailData.profileHowToVideo += '?q=completingprofiletutors'
					AppHelpers.Email.welcomeTutor(_welcomeEmailData)
				}
				else {
					_welcomeEmailData.profileHowToVideo += '?q=completingprofilestudents'
					AppHelpers.Email.welcomeStudent(_welcomeEmailData)
				}

				return AppHelpers.Utils.cRes(res, retData);
			}
		}
		catch (err) {
			AppHelpers.ErrorLogger("ERROR in users.Controller.create ", err);
			_.assign(retData, { status: "error", statusCode: "500", msg: err.message });
			return AppHelpers.Utils.cRes(res, retData);
		}
	},

	/**
	 * Account activation
	 */
	accountActivation: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();
		try {
			let _reqData = req.body;
			let _user = await Models.User.findOne({
				email: _reqData.email,
				activationCode: _reqData.code,
				status: -1
			});

			if (!_user) {
				_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_OTP });
				return AppHelpers.Utils.cRes(res, retData);
			}

			_user.activationCode = '';
			_user.activatedOn = new Date;
			_user.status = 1;

			if(!_.isEmpty(_reqData.marketing_uuid)) {
				_user.marketing_uuid = _reqData.marketing_uuid;
			}

			_user.save();

			let message = AppHelpers.ResponseMessages.ACTIVATED_SUCCESSFUL;
			// Generate token and send for login
			let token = await userHelper.getUserToken(_user);
			if (!token) {
				token = '';
				message = AppHelpers.ResponseMessages.ACTIVATED_SUCCESSFUL_TOKEN_NOT_GENERATED;
			}

			_.assign(retData, { msg: message, data: [{token: token}] });

			// get ip
			let _IP = AppHelpers.Utils.getIP(req);
			// activity log
			await Models.ActivityLog.addLog('ACCACTIVATE', _user, _IP);

			// send new welcome email
			let _welcomeEmailData = { to: _user.email, username: _user.username, profileHowToVideo: process.env.appURL + 'faqs' };
			if (_user.role[0] === 'TUT') {
				_welcomeEmailData.profileHowToVideo += '?q=completingprofiletutors'
				AppHelpers.Email.welcomeTutor(_welcomeEmailData)
			}
			else {
				_welcomeEmailData.profileHowToVideo += '?q=completingprofilestudents'
				AppHelpers.Email.welcomeStudent(_welcomeEmailData)
			}

			return AppHelpers.Utils.cRes(res, retData);
		}
		catch (err) {
			_.assign(retData, { status: "error", statusCode: "500", msg: err.message });
			return AppHelpers.Utils.cRes(res, retData);
		}
	},

	/**
	 * Re-send account activation code
	 */
	resendVerifyCode: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();
		try {
			let _reqData = req.body;
			let _user = await Models.User.findOne({
				email: _reqData.email,
				status: -1
			});
			if (!_user) {
				_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_DATA });
				return AppHelpers.Utils.cRes(res, retData);
			}

			_user.activationCode = AppHelpers.Utils.genRandomNumber();
			_user.save();

			let _welcomeEmailData = { to: _user.email, username: _user.username, code: _user.activationCode, marketingText: "" };

			AppHelpers.Email.welcome(_welcomeEmailData)

			retData.msg = "Please check your Email for the account activation code";
		}
		catch (err) {
			_.assign(retData, { status: "error", statusCode: "500", msg: err.message });
		}
		return AppHelpers.Utils.cRes(res, retData);
	},

	/**
	 * Login to the account
	 */
	login: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();

		let _reqData = req.body;
		let _filter = { status: { $ne: AppHelpers.Status.DELETED } };
		if (AppHelpers.Utils.isValidEmail(_reqData.username))
			_.merge(_filter, {email: _reqData.username});
		else
			_.merge(_filter, { username_lower: _reqData.username.toLowerCase()});

		let _user = await Models.User.findOne(_filter);
		if (!_user) {
			_.assign(retData, { status: "error", statusCode: 401, msg: AppHelpers.ResponseMessages.INVALID_EMAIL_PASS })
			return AppHelpers.Utils.cRes(res, retData);
		}

		// check user status
		let _msg = null;
		let _data = [];
		switch (_user.status) {
			// account not verified
			case -1:
				_msg = AppHelpers.ResponseMessages.ACC_NOT_VERIFIED;
				_data = [{ status: _user.status, email: _user.email }];
				break;
			// account deactivated
			case 0:
				_msg = AppHelpers.ResponseMessages.ACC_DEACTIVATED;
				_data = [{ status: _user.status }];
				break;
		}
		if (_msg) {
			_.assign(retData, { status: "error", statusCode: 401, msg: _msg, data: _data });
			return AppHelpers.Utils.cRes(res, retData);
		}

		// check password
		let passValid = await AppHelpers.Password.comparePassword(_reqData.password, _user.password);
		if (!passValid) {
			_.assign(retData, { status: "error", statusCode: 401, msg: AppHelpers.ResponseMessages.INVALID_EMAIL_PASS })
			return AppHelpers.Utils.cRes(res, retData);
		}

		let token = await userHelper.getUserToken(_user);
		if (!token) {
			_.assign(retData, { status: "error", statusCode: 500, msg: "Unable to generate token" })
			return AppHelpers.Utils.cRes(res, retData);
		}

		// get ip
		let _IP = AppHelpers.Utils.getIP(req);
		// activity log
		await Models.ActivityLog.addLog('LOGIN', _user, _IP)

		_.assign(retData, { msg: "Successfully logged in.", data: [{token: token}] })
		return AppHelpers.Utils.cRes(res, retData);
	},

	/**
	 * Forgot password check email and send mail
	 */
	forgotPassword: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();

		let _reqData = req.body;
		let _filter = { email: _reqData.email, status: { $ne: AppHelpers.Status.DELETED } };

		let _user = await Models.User.findOne(_filter);
		if (!_user) {
			_.assign(retData, { status: "error", statusCode: 404, msg: "Email does not exist." })
			return AppHelpers.Utils.cRes(res, retData);
		}

		// check user status
		let _msg = null;
		let _data = [];
		switch (_user.status) {
			// account not verified
			case -1:
				_msg = AppHelpers.ResponseMessages.ACC_NOT_VERIFIED;
				_data = [{ status: _user.status, email: _user.email }];
				break;
			// account deactivated
			case 0:
				_msg = AppHelpers.ResponseMessages.ACC_DEACTIVATED;
				_data = [{ status: _user.status }];
				break;
		}
		if (_msg) {
			_.assign(retData, { status: "error", statusCode: 401, msg: _msg, data: _data });
			return AppHelpers.Utils.cRes(res, retData);
		}

		// send email with the code and store data
		// this code will be valid for 10 mins only,
		// if validation time needs to change, then change it in the email content also
		let resetPasswordData = {
			code: AppHelpers.Utils.genRandomNumber(),
			date: moment().add(10, 'm').toDate(),
		}
		_user.resetPassword = resetPasswordData;

		if(!_.isEmpty(_reqData.marketing_uuid)) {
			_user.marketing_uuid = _reqData.marketing_uuid;
		}

		_user.save();

		AppHelpers.Email.forgotPassword({ to: _user.email, username: _user.username, code: resetPasswordData.code});

		// get ip
		let _IP = AppHelpers.Utils.getIP(req);
		// activity log
		await Models.ActivityLog.addLog('FORGOTPASS', _user, _IP)

		_.assign(retData, { msg: "Please check your email for the password reset code." })
		return AppHelpers.Utils.cRes(res, retData);
	},

	/**
	 * Verify reset password code and then send a token,
	 * then this token is required to save the new password in a new request
	 */
	resetPassword: async (req, res, next) => {
		// verificatio code will be sent along with the password
		// so first check the code the update the password
		let retData = AppHelpers.Utils.responseObject();
		try {
			let _reqData = req.body;
			let _user = await Models.User.findOne({
				email: _reqData.email,
				"resetPassword.code": _reqData.code,
				"resetPassword.date": {$gte: moment().toDate()}
			});
			if (_user) {
				_user.resetPassword.code = '';
				_user.resetPassword.date = null;
				_user.password = _reqData.password;

				if(!_.isEmpty(_reqData.marketing_uuid)) {
					_user.marketing_uuid = _reqData.marketing_uuid;
				}

				_user.save();

				_.assign(retData, { msg: "Password updated successfully." });

				// get ip
				let _IP = AppHelpers.Utils.getIP(req);
				// activity log
				await Models.ActivityLog.addLog('RESETPASS', _user, _IP);

			}
			else {
				_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_OTP });
			}
		}
		catch (err) {
			_.assign(retData, { status: "error", statusCode: 500, msg: err.message });
		}
		return AppHelpers.Utils.cRes(res, retData);
	},

	/**
	 * Get User (tutor/student) Profile data
	 *
	 */
	getProfileData: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();
		try {
			// validate and get user role
			let _role = userHelper.validateUserType({req: req});
			if (!_role) {
				_.assign(retData, {status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_REQUEST})
				return AppHelpers.Utils.cRes(res, retData);
			}

			let _selectFields = {
				username: 1, basics: 1, bio: 1, academic: 1, teachingExp: 1, hobbies: 1,
				profilePercentage: 1, alwaysOnline: 1, email: 1, usernameUpdate: 1, _id: 0,basicProfileStatus: 1, underAgeConsentData: 1
			};
			if (_role == "STU") {
				_.assign(_selectFields, { subjectInterests: 1, profilePercentageStu: 1 })
			}
			else {
				_.assign(_selectFields, { otherExp: 1, research: 1, subjectTeachings: 1, references: 1, testimonials: 1, caseStudies: 1,
					privateTutoring: 1, ratings: 1, ratingCount: 1, defaultCurrency: 1, socialLinks: 1
				})
			}

			let _user = await Models.User.findOne({_id: req.user._id}, _selectFields);

			// update the profile image and video links
			_user = userHelper.uploadProfileDataLinks(_user);

			_user.usernameUpdateStatus = _user.usernameUpdate.status;
			delete _user.usernameUpdate;

			if (_role == "STU") {
				_user.subjectInterests.data = await userHelper.getSubTreeNameFromIds({Models: Models, data: _user.subjectInterests.data});
			}
			else {
				// Removing reference link-code
				_user.references.data.forEach(record => {
					delete record.linkCode;
				});

				_user.testimonials.data.forEach((d, idx) => {
					if (!_.has(d, 'relationship')) {
						_user.testimonials.data[idx].relationship = 0;
					}

					if (!_.has(d, 'otherRelationship')) {
						_user.testimonials.data[idx].otherRelationship = '';
					}
				});

				// Add testimonials link-code
				let data = {
					username: _user.username,
					testimonials: {
						linkCode : _user.testimonials.linkCode
					}
				}
				let generatedLink = await userHelper.genTestimonialLink(data);
				_user.testimonials.linkCode = generatedLink;

				_user.subjectTeachings.data = await userHelper.getSubTreeNameFromIds({Models: Models, data: _user.subjectTeachings.data});

				if (!_.has(_user, 'ratings')) {
					_user.ratings = 0;
					_user.ratingCount = 0;
				}
			}
			let _completedSteps = userHelper.getProfileCompletedSteps(_user, _role);
			_user.completedSteps = _completedSteps;

			_.assign(retData, {data: _user})
		}
		catch (err) {
			AppHelpers.ErrorLogger("ERROR in user.Controller.getProfileData", err)
			_.assign(retData, { status: "error", statusCode: 500, msg: err.message });
		}
		return AppHelpers.Utils.cRes(res, retData);
	},

	/**
	 * Function to signin user with social account. (Google or facebook)
	 * if account already exists then, login, else create account and login
	 */
	socialSignin: async (req, res, next) => {
		let retData = AppHelpers.Utils.responseObject();
		let _reqData = req.body;
		let basics = {};

		try {
			if (_reqData) {
				let email = _reqData.email;
				if (_.isEmpty(email)) {
					let _provider = _.startCase(_.toLower(_reqData.provider));
					_.assign(retData, {
						status: "error",
						statusCode: 400,
						msg: "An email address is required. Unable to retrieve email from " + _provider
					});
					return AppHelpers.Utils.cRes(res, retData);
				}

				// Check whether given email is valid or not
				if (!AppHelpers.Utils.isValidEmail(email)){
					_.assign(retData, { status: "error", statusCode: 400, msg: AppHelpers.ResponseMessages.INVALID_EMAIL_ADDRESS })
					return AppHelpers.Utils.cRes(res, retData);
				}

				// check for the email exists or not exclude deleted
				let _filter = { email: email, status: {$ne: 9} };
				let _user = await Models.User.findOne(_filter);
				// if user exists
				if (!_.isEmpty(_user)) {
					// if user account deactivated
					if (_user.status == 0) {
						_.assign(retData, {
							status: "error",
							statusCode: 401,
							msg: AppHelpers.ResponseMessages.ACC_DEACTIVATED,
							data: [{ status: _user.status }]
						});
						return AppHelpers.Utils.cRes(res, retData);
					}

					if (_reqData.provider == 'FACEBOOK') _user.fbId = _reqData.id;
					else if (_reqData.provider == 'GOOGLE') _user.googleId = _reqData.id;

					// get user name from social data
					let _userNameData = userHelper.getUserNameFromSocialData(_reqData);

					// Update user details
					if (_.isEmpty(_user.basics.fullName) && !_.isEmpty(_userNameData.fullName)) {
						_user.basics.fullName = _userNameData.fullName;
					}
					// if first name is empty
					if (_.isEmpty(_user.basics.firstName) && !_.isEmpty(_userNameData.firstName)) {
						_user.basics.firstName = _userNameData.firstName;
					}
					// if last name is empty
					if (_.isEmpty(_user.basics.lastName) && !_.isEmpty(_userNameData.lastName)) {
						_user.basics.lastName = _userNameData.lastName;
					}

					// if account it not activated the activate it
					if (_user.status == -1) {
						_user.status = 1;
						// save activation log
					}
					// check if image loaded
					if (_.isEmpty(_user.basics.imageLink)) {
						// fetch the image and upload it to S3
						_user.basics.imageLink = await userHelper.getUserImageFromSocialData(_reqData);
					}
					_user = await _user.save();

					// genarate user token
					let token = await userHelper.getUserToken(_user);
					if (!token) {
						_.assign(retData, { status: "error", statusCode: 500, msg: "Unable to generate token" })
						return AppHelpers.Utils.cRes(res, retData);
					}

					_.assign(retData, { msg: "Successfully logged in.", data: [{token: token}] })
					return AppHelpers.Utils.cRes(res, retData);
				}
				// if user not found - register account
				else { // Register
					let _userData = {};

					// if user type empty Or not TUT/STU then assign it STU - student
					if (_.isEmpty(_reqData.type) || _.indexOf(['TUT', 'STU'], _reqData.type) === -1) {
						_reqData.type = 'STU'
					}

					_reqData.username = email.substr(0, email.indexOf('@'));
					// re-check for user name
					while (true) {
						let _usernameData = await userHelper.usernameExists(res, Object.assign({}, _reqData, {Models: Models}));
						if (_usernameData.status === "error") {
							_reqData.username = _reqData.username + AppHelpers.Utils.genRandomNumber(4);
						}
						else break;
					}

					_userData.email = _reqData.email;
					_userData.username = _reqData.username;
					_userData.role = [_reqData.type];
					_userData.status = 1;
					_userData.password = AppHelpers.Utils.getRandomString(12);

					if (_reqData.provider == 'FACEBOOK') _userData.fbId = _reqData.id;
					else if (_reqData.provider == 'GOOGLE') _userData.googleId = _reqData.id;

					// get user name from social data
					let _basics = userHelper.getUserNameFromSocialData(_reqData);
					_userData.basics = _basics;

					// fetch the image and upload it to S3
					_userData.basics.imageLink = await userHelper.getUserImageFromSocialData(_reqData);

					_user = await Models.User.create(_userData);

					if(_user.role[0] === 'TUT') {
						let uniqueVal = await AppHelpers.Utils.genRandomToken();
						_user.testimonials.linkCode = uniqueVal;
						_user.save();
					}

					// get ip
					let _IP = AppHelpers.Utils.getIP(req);
					// activity log
					await Models.ActivityLog.addLog('ACCADD', _user, _IP);

					// save default notification settings
					await notificationHelper.saveSettings({ userId: _user._id }, true);

					retData.msg = AppHelpers.ResponseMessages.REGISTRATION_WELCOME;

					// Generate token and send for login
					let token = await userHelper.getUserToken(_user);
					if (!token) {
						token = '';
						retData.msg = AppHelpers.ResponseMessages.REGISTERED_SUCCESSFUL_TOKEN_NOT_GENERATED;
					}

					_.assign(retData, { data: [{token: token}] });

					// send new welcome email
					let _welcomeEmailData = { to: _user.email, username: _user.username, profileHowToVideo: process.env.appURL + 'faqs' };
					if (_user.role[0] === 'TUT') {
						_welcomeEmailData.profileHowToVideo += '?q=completingprofiletutors'
						AppHelpers.Email.welcomeTutor(_welcomeEmailData)
					}
					else {
						_welcomeEmailData.profileHowToVideo += '?q=completingprofilestudents'
						AppHelpers.Email.welcomeStudent(_welcomeEmailData)
					}

					return AppHelpers.Utils.cRes(res, retData);
				}
			}
		}
		catch (err) {
			AppHelpers.ErrorLogger("ERROR in users.Controller.socialSignin ", err);
			_.assign(retData, { status: "error", statusCode: "500", msg: err.message });
			return AppHelpers.Utils.cRes(res, retData);
		}
	},
}

module.exports = Controller;
